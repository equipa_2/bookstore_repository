package bookstore_project;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;

/* bookstore_project version3.4 */
public class General {

	General() {

	}

	public void stop() {
		System.out.println("\t:wq");
		new java.util.Scanner(System.in).nextLine();
		return;
	}

	private String validateFile(String file_path) {
		// validating
		if (file_path == null || file_path.isEmpty() || !new File(file_path.replace("\\", "/")).isFile()
				|| file_path.length() < 4) { // 4 -> C:/a
			// System.out.println("Error: Not a File!");
			return null;
		}
		return file_path.replace("\\", "/");
	}

	private String validateDirectory(String dir_path) {
		if (dir_path == null || !new File(dir_path.replace("\\", "/")).exists() || dir_path.length() < 4) { // 4 -> C:/a
			// System.out.println("Error: Not a Dir!");
			return null;
		}
		return dir_path.replace("\\", "/");
	}

	// Check if the value is an integer
	private ArrayList<Integer> validateInteger(String IntValidate) {
		ArrayList<Integer> temparray = new ArrayList<Integer>();
		try {
			Integer value = Integer.parseInt(IntValidate);
			temparray.add(1);
			temparray.add(value);
		} catch (Exception e) {
			temparray.add(0);
			temparray.add(0);
		}
		return temparray;
	}

	public ArrayList<Book> defaultBooks(ArrayList<Book> books) {
		if (books == null || books.isEmpty() || books.size() == 0) {
			books = new ArrayList<Book>();

			// System.out.println("Books in stock!");
			// title author publisher isbn year type condition quantity price discount
			books.add(new Book("The CodeBreakers", "David Kahn", "Scribner", "9780684831305", 1996, "hardcover", "new",
					3, 59.99, 0.00));
			books.add(new Book("Procedure Writing: Principles and Practices", "Douglas Wieringa, et al.",
					"Battelle Press", "9780935470680", 1993, "paperback", "used", 15, 10.99, 1.00));
			books.add(new Book("The CERT C Secure Coding Standard", "Robert C. Seacord", "AddisonWesley Professional",
					"9780321563217", 2006, "paperback", "new", 4, 15.99, 1.00));
			books.add(new Book("Advanced UNIX Programming", "Marc J. Rochkind", "Addison-Wesley Professional",
					"9780131411548", 2004, "hardcover", "used", 1, 20.99, 1.00));
			books.add(new Book("Introduction to algorithms", "Thomas H. Cormen, et al.", "The MIT Press",
					"9780262531962", 2001, "paperback", "new", 2, 36.99, 2.00));
			books.add(new Book("t", "a", "p", "i", 2020, "t", "c", 1, 2.0, 0.01));

		}
		return books;
	}

	public void writeFile(ArrayList<Book> books, String file_path) {
		try {
			FileOutputStream fos = new FileOutputStream(file_path);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(this.defaultBooks(books));
			oos.close();
			fos.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public ArrayList<Book> readFile(String file_path) {
		ArrayList<Book> books = new ArrayList<Book>();
		try {

			FileInputStream fis = new FileInputStream(this.validateFile(file_path));
			ObjectInputStream ois = new ObjectInputStream(fis);
			books = (ArrayList) ois.readObject();
			ois.close();
			fis.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return new ArrayList<Book>();
		} catch (ClassNotFoundException c) {
			c.printStackTrace();
			return new ArrayList<Book>();
		}

		// Verify data
		// for (Book book : books) {
		// System.out.println(book.toString());
		// }

		return books;
	}

	public String readAValue(String value_request_message) {
		System.out.print(value_request_message);
		String new_value = new java.util.Scanner(System.in).nextLine();

		if (new_value.isEmpty() || new_value == null) {
			return "";
		}
		return new_value;
	}

	public Book readABook(String flag) {
		Book book = new Book();
		book.setTitle(this.readAValue("\tBook's title: "));
		book.setAuthor(this.readAValue("\tBook's author: "));
		book.setPublisher(this.readAValue("\tBook's publisher: "));
		book.setIsbn(this.readAValue("\tBook's isbn: "));
		book.setYear(validateInteger(this.readAValue("\tBook's year: ")).get(1));
		book.setType(this.readAValue("\tBook's type (e.g., hardcover or paperback): "));
		book.setCondition(this.readAValue("\tBook's condition (e.g., new or used): "));
		if (!flag.equalsIgnoreCase("add")) {
			book.setQuantity(validateInteger(this.readAValue("\tBook's quantity: ")).get(1));
		}
		book.setPrice(Double.parseDouble(this.readAValue("\tBook's price (e.g., 9.99): ").replaceAll(",", ".")));
		book.setDiscount(Double.parseDouble(this.readAValue("\tBook's discount (e.g., 0.99): ").replaceAll(",", ".")));
		return book;
	}

	public int searshForABook(ArrayList<Book> books, String todo) {
		System.out.println("\tSearch for, a book, to " + todo + ":");
		String isbn = this.readAValue("\t\tThe book's isbn is ");
		String type = this.readAValue("\t\tThe book's type (e.g., hardcover or paperback) is ");
		String condition = this.readAValue("\t\tThe book's condition (e.g., new or used) is ");
		if (!books.isEmpty() && books != null) {
			for (int i = 0; i < books.size(); i++) {
				if (books.get(i).getIsbn().equalsIgnoreCase(isbn) && books.get(i).getType().equalsIgnoreCase(type)
						&& books.get(i).getCondition().equalsIgnoreCase(condition)) {
					System.out.println("\tThe book's information was found!");
					return i;
				}
			}
		}
		System.out.println("\tThis book was not found in stock!");
		return -1;
	}

	public int searshForABookSilently(ArrayList<Book> books, String isbn, String type, String condition) {
		if (!books.isEmpty() && books != null) {
			for (int i = 0; i < books.size(); i++) {
				if (books.get(i).getIsbn().equalsIgnoreCase(isbn) && books.get(i).getType().equalsIgnoreCase(type)
						&& books.get(i).getCondition().equalsIgnoreCase(condition)) {
					System.out.println("\tThe book's information was found!");
					return i;
				}
			}
		}
		System.out.println("\tThis book was not found in stock!");
		return -1;
	}

	public String readAnEnterOrValidValue(String value_request_message, String value_type) {
		String new_value = "";
		Boolean fatal_error;
		do {
			fatal_error = false;
			System.out.print(value_request_message);
			try {
				new_value = new java.util.Scanner(System.in).nextLine();
				if (new_value != null) {
					if (new_value.isEmpty()) {
						return "Enter";
					}
					if (value_type.equalsIgnoreCase("string")) {
						return new_value;
					}
					if (value_type.equalsIgnoreCase("integer")) {
						if (Integer.parseInt(new_value) >= 0) {
							new_value = String.valueOf(Integer.parseInt(new_value));
							return new_value;
						}
						System.out.println("\t\t\tPlease enter a valid, " + value_type + "[0, 2147483647], value!");
					}
					if (value_type.equalsIgnoreCase("rational")) {
						if (Double.parseDouble(new_value.replaceAll(",", ".")) >= 0) {
							new_value = String.valueOf(Double.parseDouble(new_value.replaceAll(",", ".")));
							return new_value;
						}
						System.out.println("\t\t\tPlease enter a valid, " + value_type
								+ "[0.0, 1.79769313486231570E+308], value!");
					}
					if (value_type.equalsIgnoreCase("boolean")) {
						if (new_value.equalsIgnoreCase("true") || new_value.equalsIgnoreCase("false")) {
							new_value = Boolean.toString(Boolean.parseBoolean(new_value.toLowerCase()));
							return new_value;
						}
						System.out.println("\t\t\tPlease enter a valid, " + value_type + "[true, false], value!");
						fatal_error = true;
					}
				}
				fatal_error = true;
			} catch (Exception e) {
				fatal_error = true;
				System.out.println("\t\t\tPlease enter a valid, " + value_type + ", value!");
			}
		} while (fatal_error);
		return "fatal error";
	}

	// @SuppressWarnings("resource")
	public static void main(String[] args) {
		General run = new General();
		ArrayList<Book> books = new ArrayList<Book>();
		Book book = new Book();
		int option = 0;
		String file_name = "bookstore.data";
		String file_dir = run.validateDirectory(System.getProperty("user.dir"));
		String file_absolute_path = file_dir + "/" + file_name;

		// System.out.println("file_name: " + file_name + "\nfile_dir: " + file_dir +
		// "\nfile_absolute_path: " + file_absolute_path);
		if (run.validateFile(file_absolute_path) == null) {
			run.writeFile(books, file_absolute_path);
		}
		books = run.readFile(file_absolute_path);
		while (true) {
			option = Integer.parseInt(run.readAValue(" --------------------\n" + "|     BookStore      |\n"
					+ "|--------------------|\n" + "| [1] <show> books   |\n" + "| [2] <sell> a book  |\n"
					+ "| [3] <add> a book   |\n" + "| [4] <edit> a book  |\n" + "| [5] menu <exit>    |\n"
					+ "|--------------------|\n" + "| Select an option: "));
			System.out.println(" --------------------");

			switch (option) {
			case 1: {
				// [1] see all books
				if (!books.isEmpty() && books != null)
					for (Book i : books) {
						System.out.println("\t" + i.toString());
					}
				continue;
			}
			case 2: {
				// [2] sell a book
				int index = run.searshForABook(books, "sell");
				if (index >= 0 && books.get(index).getQuantity() > 0) {
					books.get(index).setQuantity(books.get(index).getQuantity() - 1);
					run.writeFile(books, file_absolute_path);
					System.out.println("\tThe book was sold, and its quantity was successfully updated in stock!");
				} else {
					System.out.println("\tThis book is not available for sale!");
				}
				continue;

			}
			case 3: {
				// [3] add a book
				book = run.readABook("add");
				int index = run.searshForABookSilently(books, book.getIsbn(), book.getType(), book.getCondition());
				if (index >= 0) {
					books.get(index).setQuantity(books.get(index).getQuantity() + 1);
					System.out.println("\tThe book was found, and its quantity was successfully updated in stock!");
				} else {
					book.setQuantity(1);
					books.add(book);
					System.out.println("\tA new book was successfully registered in stock!");
				}
				run.writeFile(books, file_absolute_path);
				continue;

			}
			case 4: {
				// [4] edit a book
				int index = run.searshForABook(books, "edit");
				if (index >= 0) {
					System.out.println("\tPlease press the <Enter key> to maintain an existing value!");
					String memory = run.readAnEnterOrValidValue("\t\tBook's new title: ", "String");
					books.get(index).setTitle(memory.equalsIgnoreCase("Enter") ? books.get(index).getTitle() : memory);
					memory = run.readAnEnterOrValidValue("\t\tBook's new author: ", "String");
					books.get(index)
							.setAuthor(memory.equalsIgnoreCase("Enter") ? books.get(index).getAuthor() : memory);
					memory = run.readAnEnterOrValidValue("\t\tBook's new publisher: ", "String");
					books.get(index)
							.setPublisher(memory.equalsIgnoreCase("Enter") ? books.get(index).getPublisher() : memory);
					memory = run.readAnEnterOrValidValue("\t\tBook's new isbn: ", "String");
					books.get(index).setIsbn(memory.equalsIgnoreCase("Enter") ? books.get(index).getIsbn() : memory);
					memory = run.readAnEnterOrValidValue("\t\tBook's new year: ", "Integer");
					books.get(index).setYear(
							memory.equalsIgnoreCase("Enter") ? books.get(index).getYear() : Integer.parseInt(memory));
					memory = run.readAnEnterOrValidValue("\t\tBook's new type (i.e., hardcover or paperback): ",
							"String");
					books.get(index).setType(memory.equalsIgnoreCase("Enter") ? books.get(index).getType() : memory);
					memory = run.readAnEnterOrValidValue("\t\tBook's new condition (i.e., new or used): ", "String");
					books.get(index)
							.setCondition(memory.equalsIgnoreCase("Enter") ? books.get(index).getCondition() : memory);
					memory = run.readAnEnterOrValidValue("\t\tBook's new quantity: ", "Integer");
					books.get(index).setQuantity(memory.equalsIgnoreCase("Enter") ? books.get(index).getQuantity()
							: Integer.parseInt(memory));
					memory = run.readAnEnterOrValidValue("\t\tBook's new price (e.g., 9.99): ", "Rational");
					books.get(index).setPrice(memory.equalsIgnoreCase("Enter") ? books.get(index).getPrice()
							: Double.parseDouble(memory));
					memory = run.readAnEnterOrValidValue("\t\tBook's new discount (e.g., 0.99): ", "Rational");
					books.get(index).setDiscount(memory.equalsIgnoreCase("Enter") ? books.get(index).getDiscount()
							: Double.parseDouble(memory));
					run.writeFile(books, file_absolute_path);
					System.out.println("\tThe book was successfully edited!");
				} else {
					System.out.println("\tThe book wasn't found!");
				}
				continue;
			}
			case 5: {
				// [5] exit the menu
				System.out.println("\tSee you later!");
				run.writeFile(books, file_absolute_path);
				run.stop();
				return;
			}
			default:
				break;
			}
		}

	}

}
