package bookstore_project;

import java.io.Serializable;

/* bookstore_project version3.4 */
public class Book implements Serializable {
	private static final long serialVersionUID = 1L;

	// title author publisher isbn year quantity condition type price discount
	String title, author, publisher, isbn, condition, type;
	int year, quantity;
	Double price, discount;

	public Book() {
		super();
	}

	public Book(String title, String author, String publisher, String isbn, int year, String type, String condition,
			int quantity, Double price, Double discount) {
		super();
		this.title = title;
		this.author = author;
		this.publisher = publisher;
		this.isbn = isbn;
		this.year = year;
		this.type = type;
		this.condition = condition;
		this.quantity = quantity;
		this.price = price;
		this.discount = discount;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public String getPublisher() {
		return publisher;
	}

	public String getIsbn() {
		return isbn;
	}

	public int getYear() {
		return year;
	}

	public String getType() {
		return type;
	}

	public String getCondition() {
		return condition;
	}

	public int getQuantity() {
		return quantity;
	}

	public Double getPrice() {
		return price;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public String toStringClean() {
		return "Book (" + title + ", " + author + ", " + publisher + ", " + isbn + ", " + year + ", " + type + ", "
				+ condition + ", " + quantity + ", " + price + ", " + discount + ")";
	}

	@Override
	public String toString() {
		return "Book [title=" + title + ", author=" + author + ", publisher=" + publisher + ", isbn=" + isbn + ", year="
				+ year + ", type=" + type + ", condition=" + condition + ", quantity=" + quantity + ", price=" + price
				+ ", discount=" + discount + "]";
	}

}
