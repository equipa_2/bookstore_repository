## **BookStore (bookstore_project `version3.4`)** 

### **Ambiente de Desenvolvimento:** 
* Java;
* Integrated development environment (IDE) Eclipse; e 
* Java 8.

### **Funcionalidades do Sistema:**
```sh
 -------------------- 
|     BookStore      | /* Este projeto é responsável pelo controle lógico de stock de um sistema de gestão de stock de uma 'BookStore'; */ 
|--------------------| 
| [1] <show> books   | /* Mostra todos os livros em stock na BookStore; */
| [2] <sell> a book  | /* Vende um livro, sse houver em stock, e reduz a sua quantidade; */
| [3] <add> a book   | /* Adiciona um novo livro ao stock da Bookstore, se o ele já existir incrementa a sua quantidade; */
| [4] <edit> a book  | /* Edita os dados de um livro em stock; */
| [5] menu <exit>    | /* Guarda os dados do stock, e termina o funcionamento do sistema; e */
|--------------------| 
| Select an option: _  /* Lê a opção introduzida pelo utilizador da sistema. */
 -------------------- 
```

### **Membros da Equipa 2:**
* **Bruno Felipe**, ScrumMaster, administrador do Bitbucket e Jenkins, e developer de código;
* **Diogo Vale**, Administrador das informações do projeto (BookStore), i.e., relações públicas (RP), e developer de código;
* **Nelson Vale**, Administrador da plataforma de gestão de tarefas (Trello), e developer de código;
* **Rafael Dias**, Administrador da plataforma de gestão de tarefas (Trello), e developer de código; e
* **Vasco Brito**, TeamLeader, administrador do Bitbucket e Jenkins, e developer de código.

### **Versões do Sistema:**
* **[version_1.0](https://bitbucket.org/equipa_2/bookstore_repository/src/26722501819ab7c6ee132dcd09c6eb4bde6543e6/?at=release%2Fversion_1.0)**, adicionado o código fonte do 'Livraria' na versão 1.0 para o repositório;
* **[version_1.01](https://bitbucket.org/equipa_2/bookstore_repository/src/48d100a83173b636325adc0c45e3429d937139d1/?at=release%2Fversion_1.01)**, criação de um arquivo README.md com as informações do projeto;
* **[version_1.02](https://bitbucket.org/equipa_2/bookstore_repository/src/2f2e11f43216dcb6c543c74821cf024f41242987/?at=release%2Fversion_1.02)**, migração final do 'projeto Livraria' para bookstore_project;
* **[version_1.03](https://bitbucket.org/equipa_2/bookstore_repository/src/2446326527c532fc276970a11bdeb2cafa793d30/?at=release%2Fversion_1.03)**, upload de *.jar e *.bat;
* **[version_1.04](https://bitbucket.org/equipa_2/bookstore_repository/src/f635a25def52c7daf1fe884aa5c625e2fc0547e3/?at=release%2Fversion_1.04)**, preço e desconto do livro em funcionamento;
* **[version_1.05](https://bitbucket.org/equipa_2/bookstore_repository/src/2e0ed633103732698800b900ed579659cfa503b7/?at=release%2Fversion_1.05)**, inserir editor e isbn;
* **[version_1.06](https://bitbucket.org/equipa_2/bookstore_repository/src/fbd4a26394a8e385fe1ceae57c43ad8eb1cd56a1/?at=release%2Fversion-1.06)**, criação de uma imagem de Docker (através de um Dockerfile), para criar um container com o projeto e todas as suas dependências;
* **[version_2.0](https://bitbucket.org/equipa_2/bookstore_repository/src/8edd8054cfa8ed306f54032f041f61e9e81e8f57/?at=release%2Fversion-2.0)**, novas tarefas implementadas no código "General.java" e configuração do arquivo da janela de docker-compose (*.yml);
* **[version_2.1](https://bitbucket.org/equipa_2/bookstore_repository/src/3cbebca4390d60b8b33bfa5b9d3ec8b5bfc6c28e/?at=release%2Fversion-2.1)**, alteração do docker-compose e atualização do README.md;
* **[version_3.0](https://bitbucket.org/equipa_2/bookstore_repository/src/4ec1d099ffd37770b0aab8ef3eea22be8e158984/?at=release%2Fversion3.0)**, configurando um projeto montável do Jenkins;
* **[version_3.1](https://bitbucket.org/equipa_2/bookstore_repository/src/e7a56b522cd023fbfd844ce3fb3d04c84d7bba16/?at=release%2Fversion3.1)**, dar a oportunidade ao utilizador de utilizar o artifact (BookStore_Project.zip) com o docker e docker-compose;
* **[version_3.2](https://bitbucket.org/equipa_2/bookstore_repository/src/c770a5d9dd7d20b31ccf5c8f258aa37603be6d1a/?at=release%2Fversion3.2)**, alteração do menu do código;
* **[version_3.3](https://bitbucket.org/equipa_2/bookstore_repository/src/ae8d08eb2947987c27a2d602ff95791cf5e9e1eb/?at=release%2Fversion3.3)**, nova tarefa implementada no código "General.java" e dar a oportunidade ao utilizador de utilizar o artifact (BookStore_Project.zip) com o docker e docker-compose;
* **[version_3.4](https://bitbucket.org/equipa_2/bookstore_repository/src/f7a54e3c0b853508873a93463dae74c706bc210f/?at=release%2Fversion3.4)**, validação dos números inteiros introduzidos no sistema, descrição do funcionamento das versões do sistema, e utilização da tecla 'Enter' para manter os dados do livro; e 
* **[version_4.0](https://bitbucket.org/equipa_2/bookstore_repository/src/development/)**, em desenvolvimento.  


## **Como Utilizar o Sistema**

### **Para [usar](https://labs.play-with-docker.com/) no [git](https://git-scm.com/doc):**
```sh
$ git --version
$ git config --global user.name "Your Name"
$ git config --global user.email "your_email@address.com"
$ git clone https://bitbucket.org/equipa_2/bookstore_repository.git
$ cd bookstore_repository/
$ git checkout feature/dev_YourName_YourSurname_YourNumberID
$ git pull origin development
```

### **Para [usar](https://labs.play-with-docker.com/) no [docker](https://docs.docker.com/) com um [`artefacto`](https://bitbucket.org/equipa_2/bookstore_repository/src/jenkins/build_87/bookstore_project.zip) [`compilado`](http://bookstore.westeurope.cloudapp.azure.com:8080/job/bookstore_project/87/) no [Jenkins](https://www.jenkins.io/doc/):**
```sh
$ git --version
$ git clone -b jenkins https://bitbucket.org/equipa_2/bookstore_repository.git
$ cd bookstore_repository/build_87/bookstore_project/
$ docker --version
$ docker build -t bookstore-image:version3.4 . 
$ docker run --name bookstore-container -it bookstore-image:version3.4
```

### **Para [usar](https://labs.play-with-docker.com/) no [docker-compose](https://docs.docker.com/compose/) com um [`artefacto`](https://bitbucket.org/equipa_2/bookstore_repository/src/jenkins/build_87/bookstore_project.zip) [`compilado`](http://bookstore.westeurope.cloudapp.azure.com:8080/job/bookstore_project/87/) no [Jenkins](https://www.jenkins.io/doc/):**
```sh
$ git --version
$ git clone -b jenkins https://bitbucket.org/equipa_2/bookstore_repository.git
$ cd bookstore_repository/build_87/bookstore_project/
$ docker-compose --version
$ docker-compose build
$ docker-compose up -d
$ docker exec -it bookstore-cashier-container bash
$ java -jar target/bookstore-version3.4.jar
```
