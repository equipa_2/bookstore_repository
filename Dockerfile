FROM openjdk:15-jdk
LABEL maintainer="Equipa 2"
COPY . /opt/bookstore_project
WORKDIR /opt/bookstore_project
RUN chmod 777 . -R
CMD ["java","-jar","target/bookstore-version3.4.jar"]
